import 'package:flutter/material.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;
import 'package:socket_io_client/socket_io_client.dart';

enum ServerStatus { Online, Offline, Connecting }

class SocketService with ChangeNotifier {
  ServerStatus _serverStatus = ServerStatus.Connecting;

  SocketService() {
    this._initConfig();
  }
  void _initConfig() {
    IO.Socket socket = IO.io(
        'http://192.168.1.108:3001',
        OptionBuilder()
            .setTransports(['websocket'])
            .enableAutoConnect()
            .build());

    socket.onConnect((_) {
      print('connect');
      print('_serverStatus: $_serverStatus');
    });
    socket.onDisconnect((_) => print('disconnect'));
  }
}
