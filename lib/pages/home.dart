import 'package:band_names_socket/models/band.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  static final String routerName = "home";

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Band> bands = [
    Band(id: '1', name: 'Metalica', votes: 5),
    Band(id: '2', name: 'AC', votes: 6),
    Band(id: '3', name: 'Black', votes: 3),
    Band(id: '4', name: 'Pukn', votes: 1),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        title: Text(
          'BandNames',
          style: TextStyle(color: Colors.black87),
        ),
      ),
      body: ListView.builder(
        itemCount: bands.length,
        itemBuilder: (BuildContext context, int index) {
          return _tituloBanda(bands[index]);
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: addNewBand,
        elevation: 2,
        child: Icon(Icons.add),
      ),
    );
  }

  Dismissible _tituloBanda(Band band) {
    return Dismissible(
        key: Key(band.id!),
        direction: DismissDirection.startToEnd,
        onDismissed: (DismissDirection dismissDirection) {},
        background: Container(
          padding: EdgeInsets.only(),
          color: Colors.redAccent,
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              'Eliminar Banda',
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
        child: ListTile(
          leading: CircleAvatar(
            child: Text('${band.name!.substring(0, 2)}'),
            backgroundColor: Colors.blue[100],
          ),
          title: Text('${band.name}'),
          trailing: Text(
            '${band.votes}',
            style: TextStyle(fontSize: 20),
          ),
          onTap: () => print(band.name),
        ));
  }

  addNewBand() {
    final TextEditingController textController = new TextEditingController();

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('New band name:'),
          content: TextField(
            controller: textController,
          ),
          actions: [
            MaterialButton(
              elevation: 10,
              onPressed: () => addBandToList(textController.text),
              child: Text('data'),
            ),
          ],
        );
      },
    );
  }

  void addBandToList(String name) {
    if (name.length > 1) {
      this
          .bands
          .add(new Band(id: DateTime.now().toString(), name: name, votes: 0));
      setState(() {});
    }
    Navigator.pop(context);
  }
}
