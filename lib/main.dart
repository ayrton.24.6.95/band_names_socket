import 'package:band_names_socket/pages/home.dart';
import 'package:band_names_socket/pages/status.dart';
import 'package:band_names_socket/services/socket.service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (BuildContext context) => new SocketService(),
        )
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Material App',
        initialRoute: StatusPage.routerName,
        routes: {
          HomePage.routerName: (BuildContext context) => HomePage(),
          StatusPage.routerName: (BuildContext context) => StatusPage(),
        },
      ),
    );
  }
}
